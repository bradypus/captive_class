#include "Captive.h"

using namespace std;
using namespace cv;

Captive::Captive()
{
	//init everything

	//check if the camera is open
	if (!cap.isOpened())  
	{
		std::cout << "Could not open the camera device" << endl;
		return;
	}

	/**Start to initialize Cube detection + Pose Estimation**/
	//initialize parameters
	initParamsWebcam();
	initFpsAll();
	initViewMat();
	initMasks(masks);

	//init Kalman Filter
	initKalmanFilter(KF, nStates, nMeasurements, nInputs, dt);   
	measurements.setTo(Scalar(0));

	//initialize models
	point3Dmodels = initModels();
	readMinMaxScalars(minScalars, maxScalars);

	for (int i = 0; i < 8; i++)
	{
		isValidCorner[i] = false;
	}

	

	params = initBlobParas();

	initGLMat();
}


Captive::~Captive()
{
}

void Captive::update()
{


	if (SHOWBLOB)
	{
		frameBlob = Scalar(0);
	}

	std::clock_t start1;
	double duration1;
	start1 = std::clock();

	vector<Point3f> list_points3d_model_match; // container for the 3D model coordinates
	vector<Point2f> list_points2d_scene_match; // contrainer for the recognized 2D coordinates in the scene

	params.minThreshold = (float)minThreshold;
	params.maxThreshold = (float)maxThreshold;
	params.minArea = max(1.0f, (float)minArea);
	params.minCircularity = ((float)minConvexity) / 100;
	params.minConvexity = ((float)minConvexity) / 100;
	params.minInertiaRatio = ((float)minInertiaRatio) / 100;

	// Set up detector with params
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);

	cap >> frame;

	//frame = imread(".\\data\\cubeImg2.png", CV_LOAD_IMAGE_COLOR);

	if (!frame.data)                              // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return;
	}
	resize(frame, frame_vis, Size(640, 480), 0, 0, INTER_LINEAR);

	//flip(frame, frame_vis, 1); //use the mirror frame
	//frame_vis = frame.clone();    // refresh visualisation frame

	doCrop = isCropOn && (isEstimationOn || isTrackOn);

	if (doCrop)
	{
		cv::cvtColor(frame_vis, frame_gray, CV_RGB2GRAY);

		if (isTrackOn)
		{
			// tracking 
			if (isFirstFrame)
			{
				initBGmodel(frame_gray, meanMat);
				isFirstFrame = false;
			}



			if (findObjRegion(frame_gray, meanMat, rect))
			{
				updateMasks(rect, 1, false); //index 0: mask from estimation, index 1: mask from tracking
			}
			else
			{
				updateMasks(rect, 1, true);
			}
		}



		frame_crop = applyMasks(frame_vis, masks);
	}


	Mat cropped;
	Rect bBox;

	if (doCrop)
	{

		cropWithMask(frame_crop, frame_crop, rRects, bBox);
		cv::cvtColor(frame_crop, frame_HSV, CV_BGR2HSV);
	}
	else
	{
		cv::cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);
	}



	vector<thread> threads;

	for (int i = 0; i < NUMCORNERS; i++)
	{
		threads.push_back(thread(&Captive::detectBlobs,this, frame_HSV, minScalars, maxScalars, detector, i));
	}

	if (SHOWTHRESH)
	{
		inRange(frame_HSV, Scalar(iLowH, iLowS, iLowV), Scalar(iHighH, iHighS, iHighV), frame_thresholded); //Threshold the image
	}


	//segment corners of different colors & put them into different frames 
	// Detect Corners
	for (int i = 0; i < threads.size(); i++)
	{
		if (threads[i].joinable())
		{
			threads[i].join();
		}
	}


	for (int i = 0; i < NUMCORNERS; i++)
	{
		if (isValidCorner[i])
		{

			if (doCrop){
				corners[i].pt.x += bBox.x;
				corners[i].pt.y += bBox.y;
			}
			cornersVector.push_back(corners[i]);
		}

	}

	vector<Point2f> list_points2d_inliers;

	if (numValidCorners(isValidCorner) > minPnPRANSAC)
	{
		int maxIdx = -1;
		int maxNumMatch = -1;
		vector<Mat> vecInliers_idx;
		vector<PnPProblem> pnpVec;
		//fit different models and find out the best one
		for (size_t idx = 0; idx < point3Dmodels.size(); idx++)
		{
			//add 2D coordinates

			for (int i = 0; i < NUMCORNERS; i++){
				if (isValidCorner[i])
				{
					list_points3d_model_match.push_back(point3Dmodels[idx][i]);
					list_points2d_scene_match.push_back(corners[i].pt);
				}
			}

			// -- Step 3: Estimate the pose using RANSAC approach
			Mat inliers_idx_tmp;
			PnPProblem pnp_detection_tmp(params_WEBCAM);
			pnp_detection_tmp.estimatePoseRANSAC(list_points3d_model_match, list_points2d_scene_match,
				pnpMethod, inliers_idx_tmp,
				iterationsCount, reprojectionError, confidence);
			pnpVec.push_back(pnp_detection_tmp);
			vecInliers_idx.push_back(inliers_idx_tmp);
			if (inliers_idx_tmp.rows > maxNumMatch)
			{
				maxIdx = idx;
				maxNumMatch = inliers_idx_tmp.rows;
			}

			list_points3d_model_match.clear();
			list_points2d_scene_match.clear();
		}

		Mat inliers_idx;
		inliers_idx = vecInliers_idx[maxIdx];

		for (int i = 0; i < NUMCORNERS; i++){
			if (isValidCorner[i])
			{
				list_points3d_model_match.push_back(point3Dmodels[maxIdx][i]);
				list_points2d_scene_match.push_back(corners[i].pt);
			}
		}
		pnp_detection = pnpVec[maxIdx];
		if (isDOUBLEMODEL)
		{
			isTeaPot = maxIdx == 0;
		}


		//cout << "# inliers:   " << inliers_idx.rows <<  "  No.   "<< maxIdx<< endl;
		// -- Step 4: Catch the inliers keypoints to draw
		for (int inliers_index = 0; inliers_index < inliers_idx.rows; ++inliers_index)
		{
			int n = inliers_idx.at<int>(inliers_index);         // i-inlier
			Point2f point2d = list_points2d_scene_match[n]; // i-inlier point 2D
			list_points2d_inliers.push_back(point2d);           // add i-inlier to list
		}


		// Draw inliers points 2D
		draw2DPoints(frame_vis, list_points2d_inliers, red);

		// -- Step 5: Kalman Filter



		// GOOD MEASUREMENT
		if (inliers_idx.rows >= minInliersKalman)
		{
			//correctness[0]++;
			// Get the measured translation
			Mat translation_measured(3, 1, CV_64F);
			translation_measured = pnp_detection.get_t_matrix();

			// Get the measured rotation
			Mat rotation_measured(3, 3, CV_64F);
			rotation_measured = pnp_detection.get_R_matrix();

			// fill the measurements vector
			fillMeasurements(measurements, translation_measured, rotation_measured);



			float l = 5;
			vector<Point2f> pose_points2d;
			// -- Step 6: Set estimated projection matrix
			pnp_detection.set_P_matrix(rotation_measured, translation_measured);


			//update estimated cropped rect
			RotatedRect rect_estiamted;
			findEstimatedRegion(pnp_detection, list_points3d_model, rect_estiamted);
			if (doCrop && isEstimationOn)
			{
				updateMasks(rect_estiamted, 0, false);
			}

		}
		else if (doCrop && isEstimationOn)
		{
			RotatedRect rect;
			updateMasks(rect, 0, true);
		}

		// Instantiate estimated translation and rotation
		Mat translation_estimated(3, 1, CV_64F);
		Mat rotation_estimated(3, 3, CV_64F);


		// update the Kalman filter with good measurements
		updateKalmanFilter(KF, measurements,
			translation_estimated, rotation_estimated);


		// -- Step 6: Set estimated projection matrix
		pnp_detection_est.set_P_matrix(rotation_estimated, translation_estimated);
		//houseModel.draw2Dhouse(frame_vis, pnp_detection_est, 5);

		updateViewMat(rotation_estimated, translation_estimated);

		if (SHOWBLOB)
		{
			Mat blob_vis(frameBlob);
			cv::drawKeypoints(blob_vis, cornersVector, blob_vis, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
			imshow("Color Blob", blob_vis);
			//imwrite("blob.png", blob_vis);
		}

	}
	//cv::drawKeypoints(frame_vis, cornersVector, frame_vis, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	//drawLinesRGB(frame_vis, corners, isValidCorner);
	cornersVector.clear();

	if (SHOWTHRESH)
	{
		cv::imshow("Thresholded Image", frame_thresholded);
	}


	duration1 = (std::clock() - start1) / (double)CLOCKS_PER_SEC;
	//totalduration += duration1;
	//double fps = (correctness[0] + correctness[1]) / totalduration;
	if (showAVGfps)
	{
		fpsCount = (fpsCount + 1);
		if (!fpsBeginShow && fpsCount == NAVG)
		{
			fpsBeginShow = true;
		}

		fpsCount %= NAVG;

		fpsAll[fpsCount] = 1 / duration1;


		if (fpsBeginShow)
		{
			double avg = 0;
			for (size_t i = 0; i < NAVG; i++)
			{
				avg += fpsAll[i];
			}
			avg /= (int)NAVG;
			drawFPS(frame_vis, avg, Scalar(255, 255, 255));

		}
	}
	else
	{

		drawFPS(frame_vis, 1 / duration1, Scalar(255, 255, 255));
	}
	//cv::imshow("RGB view", frame_vis);

	cvtColor(frame_vis, frame_vis, CV_BGR2RGB);

	//destroy the threads

	for (int i = 0; i < NUMCORNERS; i++)
	{
		if (threads[i].joinable())
		{
			threads[i].detach();
		}
		threads[i].~thread();
	}
	threads.clear();
	
}

SimpleBlobDetector::Params Captive::initBlobParas(){
	SimpleBlobDetector::Params params;

	//initialize values for the trackbar of blob detector
	// Change thresholds
	params.minThreshold = 0;
	params.maxThreshold = 255;

	// Filter by Area.
	params.filterByArea = true;
	params.minArea = 103;

	// Filter by Circularity
	//params.filterByCircularity = true;
	params.minCircularity = 0.01;

	// Filter by Convexity
	//params.filterByConvexity = true;
	params.minConvexity = 0.00;

	// Filter by Inertia
	params.filterByInertia = true;
	params.minInertiaRatio = 0.25;

	return params;
}

void Captive::createHSVControlBar(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV){

	//Create trackbars in "Control" window
	cvCreateTrackbar("LowH", "Control", &iLowH, 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &iHighH, 179);
	cvCreateTrackbar("LowS", "Control", &iLowS, 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &iHighS, 255);
	cvCreateTrackbar("LowV", "Control", &iLowV, 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &iHighV, 255);
}

void Captive::createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio){
	cvCreateTrackbar("minThreshold", "Control Blob Detection", &minThreshold, 255); //minThreshold (0 - 255)
	cvCreateTrackbar("maxThreshold", "Control Blob Detection", &maxThreshold, 255); //maxThreshold (0 - 255)
	cvCreateTrackbar("minArea", "Control Blob Detection", &minArea, 3000); //minArea (0 - 307200 (=640*480) )
	cvCreateTrackbar("minCircularity", "Control Blob Detection", &minCircularity, 100); //minCircularity (0-1) --- /100 to be done
	cvCreateTrackbar("minConvexity", "Control Blob Detection", &minConvexity, 100); //minConvexity (0 - 1) --- /100 to be done
	cvCreateTrackbar("minInertiaRatio", "Control Blob Detection", &minInertiaRatio, 100); //minInertiaRatio (0 - 1)

}

void Captive::detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index){
	float maxArea = -1;
	int maxIndex = -1;

	inputFrame = cv::Scalar::all(255) - inputFrame;
	detector->detect(inputFrame, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		isValidCorner[index] = false;
	}

	//drawKeypoints(inputFrame, keypoints, inputFrame, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
}

void Captive::initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{

	KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter

	setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
	setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance


	/** DYNAMIC MODEL **/

	//  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]

	// position
	KF.transitionMatrix.at<double>(0, 3) = dt;
	KF.transitionMatrix.at<double>(1, 4) = dt;
	KF.transitionMatrix.at<double>(2, 5) = dt;
	KF.transitionMatrix.at<double>(3, 6) = dt;
	KF.transitionMatrix.at<double>(4, 7) = dt;
	KF.transitionMatrix.at<double>(5, 8) = dt;
	KF.transitionMatrix.at<double>(0, 6) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(1, 7) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(2, 8) = 0.5*pow(dt, 2);

	// orientation
	KF.transitionMatrix.at<double>(9, 12) = dt;
	KF.transitionMatrix.at<double>(10, 13) = dt;
	KF.transitionMatrix.at<double>(11, 14) = dt;
	KF.transitionMatrix.at<double>(12, 15) = dt;
	KF.transitionMatrix.at<double>(13, 16) = dt;
	KF.transitionMatrix.at<double>(14, 17) = dt;
	KF.transitionMatrix.at<double>(9, 15) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(10, 16) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(11, 17) = 0.5*pow(dt, 2);


	/** MEASUREMENT MODEL **/

	//  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

	KF.measurementMatrix.at<double>(0, 0) = 1;  // x
	KF.measurementMatrix.at<double>(1, 1) = 1;  // y
	KF.measurementMatrix.at<double>(2, 2) = 1;  // z
	KF.measurementMatrix.at<double>(3, 9) = 1;  // roll
	KF.measurementMatrix.at<double>(4, 10) = 1; // pitch
	KF.measurementMatrix.at<double>(5, 11) = 1; // yaw

}

void Captive::updateKalmanFilter(KalmanFilter &KF, Mat &measurement,
	Mat &translation_estimated, Mat &rotation_estimated)
{

	// First predict, to update the internal statePre variable
	Mat prediction = KF.predict();

	// The "correct" phase that is going to use the predicted value and our measurement
	Mat estimated = KF.correct(measurement);

	// Estimated translation
	translation_estimated.at<double>(0) = estimated.at<double>(0);
	translation_estimated.at<double>(1) = estimated.at<double>(1);
	translation_estimated.at<double>(2) = estimated.at<double>(2);

	// Estimated euler angles
	Mat eulers_estimated(3, 1, CV_64F);
	eulers_estimated.at<double>(0) = estimated.at<double>(9);
	eulers_estimated.at<double>(1) = estimated.at<double>(10);
	eulers_estimated.at<double>(2) = estimated.at<double>(11);

	// Convert estimated quaternion to rotation matrix
	rotation_estimated = euler2rot(eulers_estimated);

}

void Captive::fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured)
{
	// Convert rotation matrix to euler angles
	Mat measured_eulers(3, 1, CV_64F);
	measured_eulers = rot2euler(rotation_measured);

	// Set measurement to predict
	measurements.at<double>(0) = translation_measured.at<double>(0); // x
	measurements.at<double>(1) = translation_measured.at<double>(1); // y
	measurements.at<double>(2) = translation_measured.at<double>(2); // z
	measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
	measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
	measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}

void Captive::drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner){
	// draw lines in the rgb frame to show the cube
	for (int i = 0; i < 4; i++){
		if (isValidCorner[i] && isValidCorner[(i + 1) % 4]){
			line(frame, corners[i].pt, corners[(i + 1) % 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i + 4] && isValidCorner[(i + 1) % 4 + 4])
		{
			line(frame, corners[i + 4].pt, corners[(i + 1) % 4 + 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i] && isValidCorner[i + 4])
		{
			line(frame, corners[i].pt, corners[i + 4].pt, Scalar(0, 255, 0), 3);
		}
	}

}

void Captive::initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners){
	srcCorners.push_back(Point2f(0, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, imgToDisplay.rows));
	srcCorners.push_back(Point2f(0, imgToDisplay.rows));
}

void Captive::initPointModel(vector<Point3f> &list_points3d_model){
	float length = 5.0;
	list_points3d_model.push_back(Point3f(-length, -length, length));
	list_points3d_model.push_back(Point3f(length, -length, length));
	list_points3d_model.push_back(Point3f(length, -length, -length));
	list_points3d_model.push_back(Point3f(-length, -length, -length));
	list_points3d_model.push_back(Point3f(-length, length, length));
	list_points3d_model.push_back(Point3f(length, length, length));
	list_points3d_model.push_back(Point3f(length, length, -length));
	list_points3d_model.push_back(Point3f(-length, length, -length));
}

void Captive::detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index){
	Mat frame_thresh;
	vector<KeyPoint> keypoints;

	if (index != 2)
	{
		//if not red
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner
	}
	else if (index == 2)
	{
		Mat frame_red2;
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner

		inRange(frame_HSV, mins[8], maxs[8], frame_red2); //Threshold the red corner ---- the second part
		cv::add(frame_thresh, frame_red2, frame_thresh);


	}


	float maxArea = -1;
	int maxIndex = -1;
	//blur(frame_thresh, frame_thresh, Size(9, 9), Point(-1, -1));
	GaussianBlur(frame_thresh, frame_thresh, Size(9, 9), 0, 0);
	mtx.lock();
	if (SHOWBLOB)
	{
		cv::add(frameBlob, frame_thresh, frameBlob);
	}
	mtx.unlock();

	frame_thresh = cv::Scalar::all(255) - frame_thresh;


	detector->detect(frame_thresh, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		mtx.lock();
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
		mtx.unlock();

	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		mtx.lock();
		isValidCorner[index] = false;
		mtx.unlock();
	}
	//cout << "Thread     " << index << "finished!" << endl;
}

void Captive::initMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars){
	if (minScalars.size() > 0 || maxScalars.size() > 0)
	{
		//if they are not empty vectors
		cerr << "min or max scalar vectors not empty!" << endl;
		return;
	}

	if (CAMNUM == 0)
	{
		minScalars.push_back(Scalar(36, 131, 82));			 // green
		minScalars.push_back(Scalar(6, 136, 122));			// orange
		minScalars.push_back(Scalar(0, 147, 93));			// red1
		minScalars.push_back(Scalar(21, 134, 89));			// yellow
		minScalars.push_back(Scalar(17, 114, 96));			// yellow o 
		minScalars.push_back(Scalar(110, 61, 0));			// ultramaring blue
		minScalars.push_back(Scalar(113, 47, 84));			//purple
		minScalars.push_back(Scalar(101, 173, 108));		//blue
		minScalars.push_back(Scalar(175, 107, 114));		//red

		maxScalars.push_back(Scalar(82, 252, 255));			// green
		maxScalars.push_back(Scalar(18, 213, 253));			 // orange
		maxScalars.push_back(Scalar(6, 250, 215));			// red1
		maxScalars.push_back(Scalar(47, 211, 255));			// yellow
		maxScalars.push_back(Scalar(21, 159, 255));			// yellow o 
		maxScalars.push_back(Scalar(117, 255, 255));		// ultramaring blue
		maxScalars.push_back(Scalar(129, 192, 196));		//purple
		maxScalars.push_back(Scalar(113, 234, 255));		//blue
		maxScalars.push_back(Scalar(179, 176, 255));		//red
	}
	else if (CAMNUM == 1)
	{
		minScalars.push_back(Scalar(45, 12, 0));			 // green
		minScalars.push_back(Scalar(11, 159, 17));			// orange
		minScalars.push_back(Scalar(0, 206, 19));			// red1
		minScalars.push_back(Scalar(21, 117, 0));			// yellow
		minScalars.push_back(Scalar(65, 87, 14));			// cyan
		minScalars.push_back(Scalar(115, 68, 0));			// ultramaring blue
		minScalars.push_back(Scalar(126, 80, 0));			//purple
		minScalars.push_back(Scalar(103, 164, 0));		//blue
		minScalars.push_back(Scalar(178, 208, 0));		//red

		maxScalars.push_back(Scalar(75, 150, 255));			// green
		maxScalars.push_back(Scalar(22, 255, 255));			 // orange
		maxScalars.push_back(Scalar(11, 255, 255));			// red1
		maxScalars.push_back(Scalar(45, 255, 255));			// yellow
		maxScalars.push_back(Scalar(107, 255, 255));			// cyan
		maxScalars.push_back(Scalar(123, 255, 255));		// ultramaring blue
		maxScalars.push_back(Scalar(154, 155, 255));		//purple
		maxScalars.push_back(Scalar(113, 218, 255));		//blue
		maxScalars.push_back(Scalar(179, 255, 255));		//red
	}



}

void Captive::readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars){

	ifstream iFile("lightingParams.txt");
	string line;
	if (!iFile.is_open())
	{
		cout << "Can't open lighting parameters! " << endl;
	}

	while (iFile)
	{
		if (!getline(iFile, line))
		{
			break;
		}
		stringstream ss(line);
		vector <int> record;

		int count = 0;
		while (ss){
			string s;
			if (!getline(ss, s, ','))	break;
			record.push_back(stoi(s));
		}
		minScalars.push_back(Scalar(record[0], record[1], record[2]));
		maxScalars.push_back(Scalar(record[3], record[4], record[5]));
	}
	iFile.close();

}

void Captive::updateViewMat(cv::Mat R_matrix, cv::Mat t_matrix){


	//cout << t_matrix;
	
	viewMat[0] = R_matrix.at<double>(0, 0);
	viewMat[1] = R_matrix.at<double>(1, 0);
	viewMat[2] = R_matrix.at<double>(2, 0);
	viewMat[4] = R_matrix.at<double>(0, 1);
	viewMat[5] = R_matrix.at<double>(1, 1);
	viewMat[6] = R_matrix.at<double>(2, 1);
	viewMat[8] = R_matrix.at<double>(0, 2);
	viewMat[9] = R_matrix.at<double>(1, 2);
	viewMat[10] = R_matrix.at<double>(2, 2);
	viewMat[12] = t_matrix.at<double>(0);
	viewMat[13] = t_matrix.at<double>(1);
	viewMat[14] = t_matrix.at<double>(2);
	viewMat[15] = 1;



}

bool Captive::findObjRegion(Mat frame_gray, Mat &meanMat, RotatedRect &rect){ // algorithm from "Adaptive Color Background Modeling for Real - Time Segmentation of Video Streams"

	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

	bool output = false;
	int nElements = frame_width*frame_height;

	//only consider grayscale 
	Mat diff = frame_gray - meanMat;


	//compare if (diff > standard_deviation*2 )  in other words, this part is checking if the current pixel falls in the 2*deviation of mean
	Mat binary_output;
	threshold(diff, binary_output, MINDIFF, 255, cv::THRESH_BINARY);

	/// Find minmal bounding boxes/circles http://docs.opencv.org/2.4/doc/tutorials/imgproc/shapedescriptors/bounding_rotated_ellipses/bounding_rotated_ellipses.html
	findContours(binary_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	/// Find the rotated rectangles and ellipses for each contour
	vector<RotatedRect> minRect(contours.size());
	//vector<RotatedRect> minEllipse(contours.size());

	for (int i = 0; i < contours.size(); i++)
	{
		minRect[i] = minAreaRect(Mat(contours[i]));
		/*	if (contours[i].size() > 5)
		{
		minEllipse[i] = fitEllipse(Mat(contours[i]));
		}*/
	}

	/// Draw contours + rotated rects + ellipses
	Mat drawing = Mat::zeros(binary_output.size(), CV_8UC3);
	int rectWidthMin = 5;
	int rectHeightMin = 5;
	vector<Point2f> boxPoints;
	for (int i = 0; i< contours.size(); i++)
	{

		if (minRect[i].size.width <= rectWidthMin || minRect[i].size.height <= rectHeightMin)
		{
			continue;
		}
		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		// contour
		//drawContours(drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point());
		// ellipse
		//ellipse(drawing, minEllipse[i], color, 2, 8);
		// rotated rectangle
		Point2f rect_points[4]; minRect[i].points(rect_points);
		for (int j = 0; j < 4; j++){
			line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
			boxPoints.push_back(rect_points[j]);
		}
	}

	RotatedRect minBoundingBox;
	if (boxPoints.size()>0)
	{
		minBoundingBox = minAreaRect(boxPoints);
		Point2f rect_points[4]; minBoundingBox.points(rect_points);

		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		for (int j = 0; j < 4; j++){
			line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
		}
		rect = minBoundingBox;
		rect.size.height += 20;
		rect.size.width += 20; //enlarge the area a little
		output = true;
	}




	//update the mean and standard deviation matrices
	meanMat = (1 - ALPHA)*meanMat + ALPHA*frame_gray;




	/*imshow("diff", diff);
	imshow("binary", binary_output);
	imshow("mean matrix", meanMat);*/

	//imshow("Contours", drawing);
	return output;
}

void Captive::initBGmodel(Mat frame_gray, Mat &meanMat){

	meanMat = frame_gray.clone();
}

void Captive::updateMasks( RotatedRect rect, int index, bool isReset){
	//update the mask at index with new rectangle rect;
	//if index = -1. clear the mask from estimation
	if (isReset)
	{
		if (index == 1)
		{
			masks[index] = Mat(masks[index].size(), CV_8U, Scalar(0));   // if no tracking info found, turn off the mask
			if (doCrop)
			{
				rRects[index] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));
			}


		}
		else if (index == 0)
		{
			masks[index] = Mat(masks[index].size(), CV_8U, Scalar(255)); //if no estimation found, search the whole image

			if (doCrop)
			{
				rRects[index] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(frame_width, frame_height), 0.0));
			}



		}
		else if (index == -1)
		{
			masks[0] = Mat(masks[0].size(), CV_8U, Scalar(0)); //clear the mask. Disable it
			if (doCrop)
			{
				rRects[0] = (RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));
			}
		}

		return;
	}

	Mat newMask = Mat(masks[index].size(), CV_8U, Scalar(0));
	cv::Point2f vertices2f[4];
	cv::Point vertices[4];
	rect.points(vertices2f);
	for (int i = 0; i < 4; ++i){
		vertices[i] = vertices2f[i];
	}
	cv::fillConvexPoly(newMask, vertices, 4, Scalar(255));

	masks[index] = newMask;

	//update rotated rectangles

	if (doCrop)
	{
		rRects[index] = rect;
	}


}

Mat Captive::applyMasks(Mat src, vector<Mat> masks){
	Mat dst;
	Mat finalMask;

	if (masks.size() != 2)
	{
		return src;
	}
	else
	{
		bitwise_or(masks[0], masks[1], finalMask);
		src.copyTo(dst, finalMask);
	}

	imshow("image cropped", dst);
	return dst;
}

void Captive::cropWithMask(Mat src, Mat &dst, vector<RotatedRect> rRects, Rect &bBox){

	vector<Point2f> cornerPoints;
	for (size_t i = 0; i < 2; i++)
	{
		if (rRects[i].size.width == 0 || rRects[i].size.height == 0){ //if it's not a valid rectangle
			continue;
		}
		else
		{
			cv::Point2f vertices2f[4];
			rRects[i].points(vertices2f);

			for (size_t j = 0; j < 4; j++)
			{
				cornerPoints.push_back(vertices2f[j]);
			}
		}
	}

	bBox = boundingRect(cornerPoints);

	Size rect_size = bBox.size();
	getRectSubPix(src, rect_size, Point2f((bBox.width - 1) / 2 + bBox.x, (bBox.height - 1) / 2 + bBox.y), dst);

	//rectangle(src, bBox, Scalar(255));
	//cout << bBox.x << "," <<bBox.y<<endl;
	//imshow("This src", src);
	//imshow("Cropped", dst);

}

void Captive::findEstimatedRegion(PnPProblem pnp_detection, vector<Point3f> list_points3d_model, RotatedRect &rect){


	//1. Back-project 3d points to the 2d plane
	vector<cv::Point2f> points2d;
	for (size_t i = 0; i < list_points3d_model.size(); i++)
	{
		points2d.push_back(pnp_detection.backproject3DPoint(list_points3d_model[i]));
	}

	//2. find the minimal bounding box to cover those 2d points
	RotatedRect minBoundingBox;
	minBoundingBox = minAreaRect(points2d);

	rect = minBoundingBox;
	rect.size.height *=1.4;
	rect.size.width *= 1.4; //enlarge the area a little

}

vector<Point3f> Captive::map3DCoords(vector<Point3f> in, vector<int> idx){
	//outputVec = inputVec[idx]
	vector<Point3f> out;
	for (int i = 0; i < in.size(); i++)
	{
		out.push_back(Point3f(0, 0, 0));
	}
	for (int i = 0; i < in.size(); i++)
	{
		out[i] = in[idx[i]];
	}

	return out;
}

void Captive::initMasks(vector<Mat> &masks){
	masks.push_back(Mat(Size(frame_width, frame_height), CV_8U, Scalar(255)));
	masks.push_back(Mat(Size(frame_width, frame_height), CV_8U, Scalar(0)));


	//imshow("masks",masks[i]);
	rRects.push_back(RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(frame_width, frame_height), 0.0));
	rRects.push_back(RotatedRect(Point2f(frame_width / 2, frame_height / 2), Size(0, 0), 0.0));


}

vector<vector<Point3f>> Captive::initModels(){

	vector<vector<Point3f>> output;

	initPointModel(list_points3d_model);
	int idxArray[8] = { 0, 2, 4, 6, 1, 3, 5, 7 };
	vector<int> idx;
	for (size_t i = 0; i < list_points3d_model.size(); i++)
	{
		idx.push_back(idxArray[i]);
	}

	output.push_back(list_points3d_model);
	if (isDOUBLEMODEL)
	{
		output.push_back(map3DCoords(list_points3d_model, idx));
	}

	return output;
}

void Captive::writeFPS(){
	ofstream oFile;
	oFile.open("fpsVector.txt");
	for (size_t i = 0; i < NAVG; i++)
	{
		string thisline = to_string(fpsAll[i]) + "\n";
		oFile << thisline;
	}
}

int Captive::numValidCorners(map<int, bool> isValidCorner){
	int count = 0;;
	for (int i = 0; i < NUMCORNERS; i++){
		if (isValidCorner[i])
		{
			count++;
		}
	}
	return count;
}

void Captive::getUpdateViewMat(GLdouble vm[16]){
	for (int i = 0; i < 16; i++)
	{
		vm[i] = viewMat[i];
	}
}

void Captive::getUpdateViewMatTK(GLdouble vm[16], GLdouble K){
	//amplify Translation with factor K
	for (size_t i = 0; i < 12; i++)
	{
		vm[i] = viewMat[i];
	}

	for (size_t i = 12; i < 15; i++)
	{
		vm[i] = viewMat[i] * K;
	}

	vm[15] = viewMat[15];
}

Mat Captive::getFrameVis(){
	return frame_vis;
}

void Captive::setIsCropOn(bool value){
	isCropOn = value;
}

void Captive::setIsEstimationOn(bool value){
	isEstimationOn = value;
}

void Captive::setIsTrackOn(bool value){
	isTrackOn = value;
}


void Captive::setIsTeaPot(bool value){
	isTeaPot = value;
}


bool Captive::getIsTeapot(){
	return isTeaPot;
}

double* Captive::getParams_WEBCAM(){
	return params_WEBCAM;
}

void Captive::initParamsWebcam(){
	double tmp[4]= { 584.36435949705105,   // fx
		584.36435949705105,  // fy
		319.5,      // cx
		239.5 };    // cy

	for (size_t i = 0; i < 4; i++)
	{
		params_WEBCAM[i] = tmp[i];

	}
}

void Captive::initFpsAll(){
	for (size_t i = 0; i < NAVG; i++)
	{
		fpsAll[i] = 0;
	}
}

void Captive::initViewMat(){
	for (size_t i = 0; i < 16; i++)
	{
		viewMat[i] = 0;
	}

	viewMat[0] = viewMat[5] = viewMat[10] = viewMat[15] = 1;
}

void Captive::initGLMat(){
	//reverse y - z coordinates
	for (size_t i = 0; i < 16; i++)
	{
		glMat[i] = 0;
	}
	glMat[0] = 1;
	glMat[5] = -1;
	glMat[10] = -1;
	glMat[15] = 1;

}

void Captive::getGLMat(GLdouble mat[16]){
	for (size_t i = 0; i < 16; i++)
	{
		mat[i] = glMat[i];
	}
}