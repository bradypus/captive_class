﻿/* partial code from http://www.alecjacobson.com/weblog/?p=1875
*/

// C++
#include <iostream>
#include <cstdio>
#include <ctime>
#include <map>
#include <time.h>
#include <thread>         // std::thread
#include <mutex>          // std::mutex



// Standard includes
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

//OpenGL
#include <GL/glut.h>
#include "Model_OBJ.h"

//Captive
#include "Captive.h"


using namespace std;


/** Global variables **/
Captive cp = Captive();

/** OpenGL gloabal variables **/
Model_OBJ objDragon;
Model_OBJ objBunny;
Model_OBJ objBuddha;
Model_OBJ obj;
float _angle = 0.0;
float _cameraangle = 30.0;
bool isT = false;

// angle of rotation for the camera direction
float angle = 0.0;
// actual vector representing the camera's direction
float lx = 0.0f, lz = -1.0f;
// XZ position of the camera
float x = 0.0f, z = 5.0f;

// Opencv Parameters
int frame_width = 640;
int frame_height = 480;
int frame_channels = 3;


GLint g_hWindow;

GLint   windowWidth = 640;     // Define our window width
GLint   windowHeight = 480;     // Define our window height
GLfloat fieldOfView = 45.0f;   // FoV
GLfloat zNear = 0.1f;    // Near clip plane
GLfloat zFar = 200.0f;  // Far clip plane


// Frame counting and limiting
int    frameCount = 0;

//Obj Control
float scale = 1.0;
float tValue[3] = { 0.0 };
float spin = 0;

//Merge OpenCV & OpenGL

GLdouble viewMat[16] = {0};
GLdouble glMat[16] = { 0 };
/** END of the OpenGL gloabal variables **/

/**Function Headers**/
void nameWindows();
void update(int value);

//OpenGL
GLvoid init_glut();
void processSpecialKeys(int key, int xx, int yy);
void changeSize(int w, int h);
void drawModels();
void renderScene(void);

void processNormalKeys(unsigned char key, int x, int y);





/**END of the function headers**/




int main(int argc, char **argv) {

	//init camera
	// capture properties
	
	glutInit(&argc, argv);
	

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(frame_width, frame_height);
	g_hWindow = glutCreateWindow("DEMO");
	// init GLUT and create window
	init_glut();
	
	
	
	/**initialize frames**/
	//Mat frame, frame_vis, frame_HSV, frame_thresholded;

	//vector<Mat> channels;

	//vector<Point2f> srcCorners;   // Source Points basically the 4 end co-ordinates of the overlay image
	//vector<Point2f> dstCorners;   // Destination Points to transform overlay image 

	

	//	initSrcCorners(imgToDisplay, srcCorners); //initialize the corners of the source image which is to be displayed
	//initMinMaxScalars(minScalars, maxScalars);
	

	
	//vector<KeyPoint> corners;

	nameWindows();

	// Setup SimpleBlobDetector parameters. https://github.com/spmallick/learnopencv/blob/master/BlobDetector/blob.cpp

	// Storage for blobs
	//vector<KeyPoint> keypoints;
	
	
	string objName = string(".\\Data\\titan.obj");
	obj.Load(objName);

	glutMainLoop();

	
	return 1;
}

void renderScene(void) {

	GLenum inputColourFormat = GL_RGB;
	glMatrixMode(GL_MODELVIEW);
	glDisable(GL_LIGHTING);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLuint textureID;
	glGenTextures(1, &textureID);

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, cp.getFrameVis().cols, cp.getFrameVis().rows, 0, GL_RGB, GL_UNSIGNED_BYTE, cp.getFrameVis().ptr());
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-3.2, 2.4, -5.8);

	glTexCoord2f(0.0, 1.0); glVertex3f(-3.2, -2.4, -5.8);
	glTexCoord2f(1.0, 1.0); glVertex3f(3.2, -2.4, -5.8);
	glTexCoord2f(1.0, 0.0); glVertex3f(3.2, 2.4, -5.8);


	glEnd();
	glDeleteTextures(1, &textureID);


	drawModels();
	glFlush();
	glutSwapBuffers();
}

void drawModels(){
	glMatrixMode(GL_MODELVIEW);
	glShadeModel(GL_SMOOTH);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);

	//glEnable(GL_CULL_FACE);
	glClear(GL_DEPTH_BUFFER_BIT);

	//GLfloat light_position[] = { 0.0,10.0,0.0, 0.5f };
	//glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	/*glPushMatrix();
	//spin += 10;
	//glRotated(spin, 1.0, 0.0, 0.0);
	glMultMatrixd(viewMat);
	GLfloat light_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_diffuse[] = { 0.2, 0.2,0.2, 1.0 };
	GLfloat light_specular[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };

	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);

	glEnable(GL_LIGHT0);

	GLfloat light1_ambient[] = { 0.25,	0.20725,	0.20725, 1.0 };
	GLfloat light1_diffuse[] = { 0.0, 0.4, 0.0, 1.0 };
	GLfloat light1_specular[] = {0.0, 0.2, 0.0, 1.0 };
	GLfloat light1_position[] = { 10.0, 0.0, -10.0, 1.0 };

	glLightfv(GL_LIGHT1, GL_AMBIENT, light1_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light1_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light1_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light1_position);

	glEnable(GL_LIGHT1);


	glPopMatrix();*/

	if (isT)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else
	{
		glDisable(GL_BLEND);
	}


	/*GLfloat ambientColor[] = { 0.135,	0.2225,	0.1575,0.7};
	GLfloat diffuseColor[] = { 0.54,	0.89,	0.63, 0.6};
	GLfloat specularColor[] = { 0.316228,	0.316228,	0.316228,0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };*/


	/*GLfloat ambientColor[] = { 0.1745,	0.01175,	0.01175, 0.7 };
	GLfloat diffuseColor[] = { 0.61424,	0.04136	,0.04136, 0.6 };
	GLfloat specularColor[] = { 0.727811	,0.626959	,0.626959, 0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };*/

	GLfloat ambientColor[] = { 0.0, 0.05, 0.0, 0.7 };
	GLfloat diffuseColor[] = { 0.4, 0.5, 0.4, 0.6 };
	GLfloat specularColor[] = { 0.04, 0.7, 0.04, 0.5 };
	float mat_emission[] = { 0.3f, 0.2f, 0.2f, 0.0f };


	glMaterialfv(GL_FRONT, GL_AMBIENT, ambientColor);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuseColor);
	glMaterialfv(GL_FRONT, GL_SPECULAR, specularColor);
	glMaterialf(GL_FRONT, GL_SHININESS, 0.1*128.0f);
	//glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);

	/*
	GLfloat amb_light[] = { 0.1, 0.1, 0.1, 0.1 };
	GLfloat diffuse[] = { 0.6, 0.6, 0.6, 0.3 };
	GLfloat specular[] = { 0.7, 0.7, 0.3, 0.7 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, amb_light);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular);*/


	//glEnable(GL_COLOR_MATERIAL);
	//glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	//glDepthFunc(GL_LEQUAL);
	glPushMatrix();
	//angle += 1;
	//glRotatef(angle, 0, 1, 0);
	cp.getGLMat(glMat);
	glMultMatrixd(glMat);
	glMultMatrixd(viewMat);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	if (cp.getIsTeapot())
	{
		glutSolidTeapot(2.5f);
	}
	else
	{
		glTranslatef(tValue[0], tValue[1], tValue[2]);
		glScaled(scale, scale, scale);
		obj.Draw();
	}

	//drawMyCube(5);
	glPopMatrix();

	glDisable(GL_BLEND);
}

void processNormalKeys(unsigned char key, int x, int y) {

	if (key == 27){
		//obj.Release();
		objDragon.Release();

		if (WRITEFPS)
		{
			cp.writeFPS();
		}

		exit(0);
	}
	if (key == 32)
		cout << "space pressed!" << endl;
	//if (key == 'q'){ //write frame_vis
	//	vector<int> compression_params;
	//	compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
	//	compression_params.push_back(9);
	//	Mat frame_save;
	//	//cvtColor(frame_vis,frame_save,)
	//	imwrite("frame_vis.png", frame_vis, compression_params);
	//}
	if (key == 'a'){
		cp.setIsCropOn(true);
	}
	if (key == 's'){
		cp.setIsCropOn(false);
	}
	if (key == 'e'){
		cp.setIsEstimationOn(true);
	}
	if (key == 'r'){
		//isEstimationOn = false;
		//RotatedRect tmp;
		//updateMasks(masks, tmp, -1, true);
	}
	if (key == 't'){
		cp.setIsTrackOn(true);
	}
	if (key == 'y'){
		cp.setIsTrackOn(false);
		RotatedRect tmp;
		cp.updateMasks(tmp, 1, true);
	}
	if (key == 'f')
	{
		cp.setIsTeaPot(!cp.getIsTeapot());

	}
	if (key == 'g'){
		isT = !isT;
	}
	if (key == 'z'){
		scale += 0.05;

	}
	if (key == 'x'){
		scale -= 0.05;

	}
	if (key == 'i')
	{
		tValue[1]++;

	}
	if (key == 'k')
	{
		tValue[1]--;

	}
	if (key == 'j')
	{
		tValue[0]--;

	}
	if (key == 'l')
	{
		tValue[0]++;

	}
	if (key == 'n')
	{
		tValue[2]++;

	}
	if (key == 'm')
	{
		tValue[2]--;

	}

}

void nameWindows(){

	if (SHOWBLOB)
	{
		namedWindow("Control Blob Detection", CV_WINDOW_AUTOSIZE);
		namedWindow("Color Blob", 1);
	}

	//create windows to show frames
	/*namedWindow("H Channel", 1);
	namedWindow("S Channel", 1);
	namedWindow("V Channel", 1);*/

	if (SHOWTHRESH)
	{
		namedWindow("Thresholded Image", 1);
		namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
	}

	/*namedWindow("Green Corner", 1);
	namedWindow("Blue Corner", 1);
	namedWindow("Yellow Corner", 1);
	namedWindow("Purple Corner", 1);
	namedWindow("Red Corner", 1);
	namedWindow("Orange Corner", 1);*/
//	namedWindow("RGB view", CV_WINDOW_AUTOSIZE);
}

GLvoid init_glut()
{
	glClearColor(0.0, 0.0, 0.0, 0.0);


	// register callbacks
	glutDisplayFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	glutTimerFunc(TIMEINTERVAL,update, 0);

}

void processSpecialKeys(int key, int xx, int yy) {

	float fraction = 0.1f;

	switch (key) {
	case GLUT_KEY_LEFT:
		angle -= 0.01f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_RIGHT:
		angle += 0.01f;
		lx = sin(angle);
		lz = -cos(angle);
		break;
	case GLUT_KEY_UP:
		x += lx * fraction;
		z += lz * fraction;
		break;
	case GLUT_KEY_DOWN:
		x -= lx * fraction;
		z -= lz * fraction;
		break;
	}
}

void setProjection(){
	//https://www.safaribooksonline.com/library/view/programming-computer-vision/9781449341916/ch04.html

	double fx = cp.getParams_WEBCAM()[0];
	double fy = cp.getParams_WEBCAM()[1];
	double fovy = 2 * atan(0.5*frame_height / fy) * 180 / CV_PI;
	double aspect = (frame_width*fy) / (frame_height*fx);

//	set perspective
	gluPerspective(fovy, aspect, 0.1f, 100.0f);
}

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;

	float ratio = w * 1.0 / h;

	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);

	// Reset Matrix
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	setProjection();

	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void update(int value){
	cp.update();
	//cp.getUpdateViewMat(viewMat);
	cp.getUpdateViewMatTK(viewMat,1.0);
	glutPostRedisplay();
	glutTimerFunc(TIMEINTERVAL,update, 0);
}
