This is the software part of the CAPTIVE project. This repository includes all the necessary source code to build this project.

CAPTIVE is a 6DoF input device to manipulate 3D virtual objects.

# How does it work
The system also needs a RGB camera and a colored wireframe cube. The system recognizes the cube through the input frames of the camera and estimates the pose of the cube in real time. After that we will get the 3D coordinate system with respect to that cube. 3D objects can then be rendered in the coordinate system accordingly.

# Environment Configuration

    1. Install OpenCV and make sure it works on your machine. Check this link for more details:
    http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html

    2. Install the TinyLoader
    https://syoyo.github.io/tinyobjloader/

    3. Install Visual Studio 2013 or later version

# How to setup
    
    1. Download source code.
    2. Create a new blank c++ Win32 Console Application in VS2013.
    3. Add all the source code to that project.
    4. Add the property sheet (Opencv_OpenGL.props) to that project.
    5. Build and run.