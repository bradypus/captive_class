﻿#pragma once
// C++
#include <iostream>
#include <cstdio>
#include <ctime>
#include <map>
#include <time.h>
#include <thread>         // std::thread
#include <mutex>          // std::mutex
#include <fstream>


// Standard includes
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>

// PnP Problem
#include "Mesh.h"
#include "Model.h"
#include "PnPProblem.h"
#include "RobustMatcher.h"
#include "ModelRegistration.h"
#include "Utils.h"

//3D models
//#include "House.h"

#define NUMCORNERS 8
#define SHOWTHRESH false
#define SHOWBLOB false
#define CAMNUM 1
#define SHOWCAMVEC false
#define MINDIFF 30
#define	ALPHA 0.8
#define WRITEFPS false
#define NAVG 10
#define TIMEINTERVAL 16
#define isDOUBLEMODEL false

//OpenGL
#include <GL/glut.h>


using namespace cv;
using namespace std;


class Captive
{
public:
	Captive();
	~Captive();


	void update();
	void getUpdateViewMat(GLdouble vm[16]);
	void getUpdateViewMatTK(GLdouble vm[16], GLdouble K);
	void getGLMat(GLdouble glMat[16]);
	Mat getFrameVis();

	void updateMasks(RotatedRect rect, int index, bool isReset);

	void writeFPS();

	//interface
	void setIsCropOn(bool value);
	void setIsEstimationOn(bool value);
	void setIsTrackOn(bool value);
	void setIsTeaPot(bool value);
	bool getIsTeapot();
	double* getParams_WEBCAM();

private:
	/**  GLOBAL VARIABLES  **/

	//OpenGL
	int frame_width = 640;
	int frame_height = 480;
	bool isTeaPot = false;

	//opengl interface
	GLdouble viewMat[16];
	GLdouble glMat[16];
	/**  blob experiments**/
	Mat frameBlob = Mat(480, 640, CV_8U);

	//corners and their valid map
	KeyPoint corners[NUMCORNERS];
	map<int, bool> isValidCorner;

	// Kalman Filter parameters
	int minInliersKalman = 3;    // Kalman threshold updating
	int minPnPRANSAC = 0;

	// PnP parameters
	int pnpMethod = SOLVEPNP_P3P;

	Matx33d _K = Matx33d(584.36435949705105, 0.0, 319.5,
		0, 584.36435949705105, 239.5,
		0, 0, 1);

	// RANSAC parameters
	int iterationsCount = 100;      // number of Ransac iterations.
	float reprojectionError = 2.0;  // maximum allowed distance to consider it an inlier.
	double confidence = 0.95;        // ransac successful confidence.

	// Some basic colors
	Scalar red = Scalar(0, 0, 255);
	Scalar green = Scalar(0, 255, 0);
	Scalar blue = Scalar(255, 0, 0);
	Scalar yellow = Scalar(0, 255, 255);



	//Multithreading
	std::mutex mtx;           // mutex for critical section

	//Originally from the main function of source.cpp
	SimpleBlobDetector::Params params;// = initBlobParas();
	int minThreshold = 239;
	int maxThreshold = 250;
	int minArea = 288;
	int minCircularity = 1;
	int minConvexity = 0;
	int minInertiaRatio = 50;

	//initialize values for the trackbar of color parameters
	int iLowH = 0;
	int iHighH = 179;
	int iLowS = 0;
	int iHighS = 255;
	int iLowV = 0;
	int iHighV = 255;

	// scalars for color thresholding
	vector<Scalar> minScalars, maxScalars;
	vector<Point3f> list_points3d_model; // container for the 3D model coordinates


	vector<KeyPoint> cornersVector;
	double params_WEBCAM[4];
	/** variables for the pnp problem**/
	PnPProblem pnp_detection = PnPProblem(params_WEBCAM);
	PnPProblem pnp_detection_est = PnPProblem(params_WEBCAM);

	//Kalman Filter Parameters
	KalmanFilter KF;         // instantiate Kalman Filter
	int nStates = 18;            // the number of states
	int nMeasurements = 6;       // the number of measured states
	int nInputs = 0;             // the number of control actions
	double dt = 10;           // time between measurements (1/FPS)
	Mat measurements = Mat(nMeasurements, 1, CV_64F);

	//House houseModel;

	VideoCapture cap = VideoCapture(1);
	Mat frame, frame_vis, frame_HSV, frame_thresholded, frame_gray, frame_crop;


	//Merge OpenCV & OpenGL
	cv::Mat cameraLoc = cv::Mat(4, 1, CV_64FC1);

	//Masking Variables
	vector<Mat> masks;
	vector<RotatedRect> rRects;
	RNG rng = RNG(12345);
	bool doCrop = true;
	bool isCropOn = true;
	bool isEstimationOn = true;
	bool isTrackOn = true;
	vector<vector<Point3f>> point3Dmodels;
	double fpsAll[NAVG];
	bool fpsBeginShow = false;
	int fpsCount = -1;
	bool showAVGfps = true;
	bool isFirstFrame = true;
	Mat meanMat;
	RotatedRect rect;

	/**	END of the global variables**/

	/**Function Headers**/

	SimpleBlobDetector::Params initBlobParas();

	void createHSVControlBar(int &iLowH, int &iHighH, int &iLowS, int &iHighS, int &iLowV, int &iHighV);
	void createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio);
	void detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index);

	void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
	void updateKalmanFilter(KalmanFilter &KF, Mat &measurements,
		Mat &translation_estimated, Mat &rotation_estimated);
	void fillMeasurements(Mat &measurements,
		const Mat &translation_measured, const Mat &rotation_measured);

	void drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner);
	void initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners);

	void initPointModel(vector<Point3f> &list_points3d_model);
	void detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index);
	void initMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);
	void readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);


	void updateViewMat(cv::Mat R_matrix, cv::Mat t_matrix);

	//mask operations
	bool findObjRegion(Mat frame_gray, Mat &meanMat, RotatedRect &rect);
	void initBGmodel(Mat frame_gray, Mat &meanMat);

	
	Mat applyMasks(Mat src, vector<Mat> masks);
	void cropWithMask(Mat src, Mat &dst, vector<RotatedRect> rRects, Rect &bBox);

	void findEstimatedRegion(PnPProblem pnp_detection, vector<Point3f> list_points3d_model, RotatedRect &rect);
	vector<Point3f> map3DCoords(vector<Point3f> in, vector<int> idx);
	void initMasks(vector<Mat> &masks);
	vector<vector<Point3f>> initModels();

	int numValidCorners(map<int, bool> isValidCorner);

	void initParamsWebcam();
	void initFpsAll();
	void initViewMat();
	//Transform to opengl
	void initGLMat();
};

