
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "PnPProblem.h"

using namespace std;
//using namespace cv;

cv::Scalar righthouse(179, 222, 245);
cv::Scalar rooffronthouse(0, 0, 255);
cv::Scalar lefthouse(179, 222, 245);
cv::Scalar bottomhouse(0, 0, 0);
cv::Scalar wallhouse(220, 248, 255);
cv::Scalar backhouse(140, 180, 210);
cv::Scalar roofbackhouse(34, 34, 178);
cv::Scalar doorhouse(45, 82, 160);
cv::Scalar windowhouse(255, 255, 224);

class House
{
public:
	House();
	void draw2Dhouse(cv::Mat, PnPProblem, int);
	~House();

private:
	vector<cv::Point3f> vertices;
	vector<int> indices;
	vector<cv::Scalar> colors;
};

vector<cv::Point2i> toTriangles(vector<cv::Point2f> pose_points2d, vector<int> indices, int sIndex){
	vector<cv::Point2i> triVertices;
	for (int i = 0; i < 3; i++)
	{
		triVertices.push_back(pose_points2d[indices[sIndex+i]]);
	}
	return triVertices;
}

bool isFrontFace(vector<cv::Point2i> triVertices){
	
	cv::Point3f ab = cv::Point3f(triVertices[1].x, triVertices[1].y, 0) - cv::Point3f(triVertices[0].x, triVertices[0].y, 0);
	cv::Point3f ac = cv::Point3f(triVertices[2].x, triVertices[2].y, 0) - cv::Point3f(triVertices[0].x, triVertices[0].y, 0);
	cv::Point3f n = ab.cross(ac);
	return n.z < 0;
}

House::House()
{
	vertices.push_back(cv::Point3f(-0.5, -0.5, 0.5));
	vertices.push_back(cv::Point3f(0.5, -0.5, 0.5));
	vertices.push_back(cv::Point3f(0.5, -0.5, -0.5));
	vertices.push_back(cv::Point3f(-0.5, -0.5, -0.5));
	vertices.push_back(cv::Point3f(-0.5, 0.5, 0.5));
	vertices.push_back(cv::Point3f(0.5, 0.5, 0.5));
	vertices.push_back(cv::Point3f(0.5, 0.5, -0.5));
	vertices.push_back(cv::Point3f(-0.5, 0.5, -0.5));
	vertices.push_back(cv::Point3f(-0.5, 0.75, 0));
	vertices.push_back(cv::Point3f(0.5, 0.75, 0));
	vertices.push_back(cv::Point3f(0, -0.5, 0.5));
	vertices.push_back(cv::Point3f(0.25, -0.5, 0.5));
	vertices.push_back(cv::Point3f(0, 0, 0.5));
	vertices.push_back(cv::Point3f(0.25, 0, 0.5));
	vertices.push_back(cv::Point3f(-0.5, -0.1, -0.4));
	vertices.push_back(cv::Point3f(-0.5, -0.1, 0.1));
	vertices.push_back(cv::Point3f(-0.5, 0.4, -0.4));
	vertices.push_back(cv::Point3f(-0.5, 0.4, 0.1));

	int initIndex[] = { 0, 3, 2, 0, 2, 1,	//bottom	
		0, 10, 12, 0, 12, 4, 4, 12, 5, 12, 13, 5, 13, 1, 5, 13, 11, 1,	//front	wall		
		10, 11, 13, 10, 13, 12, //front door 
		1, 2, 6, 1, 6, 5,	//right		
		2, 3, 6, 3, 7, 6,	//back		
		3, 14, 16, 3, 16, 7, 16, 4, 7, 16, 17, 4, 17, 15, 4, 15, 0, 4, 15, 3, 0, 3, 15, 14, 	//left wall	
		14, 15, 17, 14, 17, 16,		//left window	
		4, 5, 8, 5, 9, 8,	//roof front	
		5, 6, 9,			//roof right	
		6, 7, 9, 7, 8, 9,	//roof back		
		7, 4, 8 };			//roof left		
	

	colors.push_back(bottomhouse);
	colors.push_back(bottomhouse);
	colors.push_back(wallhouse);
	colors.push_back(wallhouse);
	colors.push_back(wallhouse);
	colors.push_back(wallhouse);
	colors.push_back(wallhouse);
	colors.push_back(wallhouse);
	colors.push_back(doorhouse);
	colors.push_back(doorhouse);
	colors.push_back(righthouse);
	colors.push_back(righthouse);
	colors.push_back(backhouse);
	colors.push_back(backhouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(lefthouse);
	colors.push_back(windowhouse);
	colors.push_back(windowhouse);
	colors.push_back(rooffronthouse);
	colors.push_back(rooffronthouse);
	colors.push_back(righthouse);
	colors.push_back(roofbackhouse);
	colors.push_back(roofbackhouse);
	colors.push_back(lefthouse);

	for (int i = 0; i < colors.size() * 3; i++)
	{
		indices.push_back(initIndex[i]);
	}

}



void House::draw2Dhouse(cv::Mat frame, PnPProblem pnp_detection, int magnitude){
	vector<cv::Point2f> pose_points2d;

	//backproject 3D points to 2D points
	for (int i = 0; i < vertices.size(); i++)
	{
		cv::Point3f t = vertices[i] * magnitude;
		pose_points2d.push_back(pnp_detection.backproject3DPoint(t));  // axis center
	}

	//for each triangle
	for (int i = 0; i < colors.size(); i++)
	{
		
		vector<cv::Point2i> triVertices = toTriangles(pose_points2d, indices, i * 3);
			if (isFrontFace(triVertices))
			{
				fillConvexPoly(frame, triVertices, colors[i]);
			}
			
		
	}

	//fillConvexPoly(frame, pose_points2d,3, Scalar(0),8,0);
}

House::~House()
{
}